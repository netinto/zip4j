# Zip4j 1.3.2.1 #

This Repository forked zip4j. (http://www.lingala.net/zip4j/index.php)

### Key features ###

* Create, Add, Extract, Update, Remove files from a Zip file
* Read/Write password protected Zip files
* Supports AES 128/256 Encryption
* Supports Standard Zip Encryption
* Supports Zip64 format
* Supports Store (No Compression) and Deflate compression method
* Create or extract files from Split Zip files (Ex: z01, z02,...zip)
* Supports Unicode file names
* Progress Monitor

### Bug Fix ###

* ArrayIndexOutOfBoundsException when some apk files.
* Excluding hidden files from zip also skips normal files. (By stannous : http://www.lingala.net/zip4j/forum/index.php?topic=395.0)